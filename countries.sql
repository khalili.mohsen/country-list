INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (1, 'Afghanistan', 'AF', '+93', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (2, 'Aland Islands', 'AX', '+358', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (3, 'Albania', 'AL', '+355', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (4, 'Algeria', 'DZ', '+213', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (5, 'AmericanSamoa', 'AS', '+1 684', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (6, 'Andorra', 'AD', '+376', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (7, 'Angola', 'AO', '+244', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (8, 'Anguilla', 'AI', '+1 264', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (9, 'Antarctica', 'AQ', '+672', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (10, 'Antigua and Barbuda', 'AG', '+1268', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (11, 'Argentina', 'AR', '+54', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (12, 'Armenia', 'AM', '+374', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (13, 'Aruba', 'AW', '+297', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (14, 'Australia', 'AU', '+61', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (15, 'Austria', 'AT', '+43', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (16, 'Azerbaijan', 'AZ', '+994', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (17, 'Bahamas', 'BS', '+1 242', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (18, 'Bahrain', 'BH', '+973', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (19, 'Bangladesh', 'BD', '+880', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (20, 'Barbados', 'BB', '+1 246', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (21, 'Belarus', 'BY', '+375', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (22, 'Belgium', 'BE', '+32', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (23, 'Belize', 'BZ', '+501', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (24, 'Benin', 'BJ', '+229', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (25, 'Bermuda', 'BM', '+1 441', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (26, 'Bhutan', 'BT', '+975', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (27, 'Bolivia, Plurinational State of', 'BO', '+591', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (28, 'Bosnia and Herzegovina', 'BA', '+387', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (29, 'Botswana', 'BW', '+267', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (30, 'Brazil', 'BR', '+55', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (31, 'British Indian Ocean Territory', 'IO', '+246', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (32, 'Brunei Darussalam', 'BN', '+673', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (33, 'Bulgaria', 'BG', '+359', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (34, 'Burkina Faso', 'BF', '+226', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (35, 'Burundi', 'BI', '+257', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (36, 'Cambodia', 'KH', '+855', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (37, 'Cameroon', 'CM', '+237', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (38, 'Canada', 'CA', '+1', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (39, 'Cape Verde', 'CV', '+238', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (40, 'Cayman Islands', 'KY', '+ 345', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (41, 'Central African Republic', 'CF', '+236', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (42, 'Chad', 'TD', '+235', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (43, 'Chile', 'CL', '+56', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (44, 'China', 'CN', '+86', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (45, 'Christmas Island', 'CX', '+61', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (46, 'Cocos (Keeling) Islands', 'CC', '+61', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (47, 'Colombia', 'CO', '+57', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (48, 'Comoros', 'KM', '+269', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (49, 'Congo', 'CG', '+242', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (50, 'Congo, The Democratic Republic of the Congo', 'CD', '+243', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (51, 'Cook Islands', 'CK', '+682', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (52, 'Costa Rica', 'CR', '+506', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (53, 'Cote d\'Ivoire', 'CI', '+225', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (54, 'Croatia', 'HR', '+385', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (55, 'Cuba', 'CU', '+53', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (56, 'Cyprus', 'CY', '+357', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (57, 'Czech Republic', 'CZ', '+420', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (58, 'Denmark', 'DK', '+45', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (59, 'Djibouti', 'DJ', '+253', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (60, 'Dominica', 'DM', '+1 767', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (61, 'Dominican Republic', 'DO', '+1 849', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (62, 'Ecuador', 'EC', '+593', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (63, 'Egypt', 'EG', '+20', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (64, 'El Salvador', 'SV', '+503', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (65, 'Equatorial Guinea', 'GQ', '+240', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (66, 'Eritrea', 'ER', '+291', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (67, 'Estonia', 'EE', '+372', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (68, 'Ethiopia', 'ET', '+251', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (69, 'Falkland Islands (Malvinas)', 'FK', '+500', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (70, 'Faroe Islands', 'FO', '+298', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (71, 'Fiji', 'FJ', '+679', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (72, 'Finland', 'FI', '+358', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (73, 'France', 'FR', '+33', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (74, 'French Guiana', 'GF', '+594', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (75, 'French Polynesia', 'PF', '+689', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (76, 'Gabon', 'GA', '+241', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (77, 'Gambia', 'GM', '+220', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (78, 'Georgia', 'GE', '+995', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (79, 'Germany', 'DE', '+49', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (80, 'Ghana', 'GH', '+233', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (81, 'Gibraltar', 'GI', '+350', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (82, 'Greece', 'GR', '+30', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (83, 'Greenland', 'GL', '+299', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (84, 'Grenada', 'GD', '+1 473', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (85, 'Guadeloupe', 'GP', '+590', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (86, 'Guam', 'GU', '+1 671', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (87, 'Guatemala', 'GT', '+502', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (88, 'Guernsey', 'GG', '+44', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (89, 'Guinea', 'GN', '+224', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (90, 'Guinea-Bissau', 'GW', '+245', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (91, 'Guyana', 'GY', '+595', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (92, 'Haiti', 'HT', '+509', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (93, 'Holy See (Vatican City State)', 'VA', '+379', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (94, 'Honduras', 'HN', '+504', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (95, 'Hong Kong', 'HK', '+852', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (96, 'Hungary', 'HU', '+36', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (97, 'Iceland', 'IS', '+354', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (98, 'India', 'IN', '+91', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (99, 'Indonesia', 'ID', '+62', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (100, 'Iran, Islamic Republic of Persian Gulf', 'IR', '+98', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (101, 'Iraq', 'IQ', '+964', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (102, 'Ireland', 'IE', '+353', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (103, 'Isle of Man', 'IM', '+44', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (104, 'Israel', 'IL', '+972', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (105, 'Italy', 'IT', '+39', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (106, 'Jamaica', 'JM', '+1 876', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (107, 'Japan', 'JP', '+81', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (108, 'Jersey', 'JE', '+44', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (109, 'Jordan', 'JO', '+962', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (110, 'Kazakhstan', 'KZ', '+7 7', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (111, 'Kenya', 'KE', '+254', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (112, 'Kiribati', 'KI', '+686', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (113, 'Korea, Democratic People\'s Republic of Korea', 'KP', '+850', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (114, 'Korea, Republic of South Korea', 'KR', '+82', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (115, 'Kuwait', 'KW', '+965', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (116, 'Kyrgyzstan', 'KG', '+996', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (117, 'Laos', 'LA', '+856', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (118, 'Latvia', 'LV', '+371', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (119, 'Lebanon', 'LB', '+961', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (120, 'Lesotho', 'LS', '+266', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (121, 'Liberia', 'LR', '+231', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (122, 'Libyan Arab Jamahiriya', 'LY', '+218', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (123, 'Liechtenstein', 'LI', '+423', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (124, 'Lithuania', 'LT', '+370', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (125, 'Luxembourg', 'LU', '+352', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (126, 'Macao', 'MO', '+853', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (127, 'Macedonia', 'MK', '+389', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (128, 'Madagascar', 'MG', '+261', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (129, 'Malawi', 'MW', '+265', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (130, 'Malaysia', 'MY', '+60', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (131, 'Maldives', 'MV', '+960', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (132, 'Mali', 'ML', '+223', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (133, 'Malta', 'MT', '+356', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (134, 'Marshall Islands', 'MH', '+692', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (135, 'Martinique', 'MQ', '+596', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (136, 'Mauritania', 'MR', '+222', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (137, 'Mauritius', 'MU', '+230', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (138, 'Mayotte', 'YT', '+262', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (139, 'Mexico', 'MX', '+52', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (140, 'Micronesia, Federated States of Micronesia', 'FM', '+691', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (141, 'Moldova', 'MD', '+373', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (142, 'Monaco', 'MC', '+377', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (143, 'Mongolia', 'MN', '+976', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (144, 'Montenegro', 'ME', '+382', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (145, 'Montserrat', 'MS', '+1664', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (146, 'Morocco', 'MA', '+212', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (147, 'Mozambique', 'MZ', '+258', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (148, 'Myanmar', 'MM', '+95', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (149, 'Namibia', 'NA', '+264', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (150, 'Nauru', 'NR', '+674', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (151, 'Nepal', 'NP', '+977', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (152, 'Netherlands', 'NL', '+31', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (153, 'Netherlands Antilles', 'AN', '+599', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (154, 'New Caledonia', 'NC', '+687', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (155, 'New Zealand', 'NZ', '+64', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (156, 'Nicaragua', 'NI', '+505', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (157, 'Niger', 'NE', '+227', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (158, 'Nigeria', 'NG', '+234', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (159, 'Niue', 'NU', '+683', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (160, 'Norfolk Island', 'NF', '+672', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (161, 'Northern Mariana Islands', 'MP', '+1 670', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (162, 'Norway', 'NO', '+47', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (163, 'Oman', 'OM', '+968', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (164, 'Pakistan', 'PK', '+92', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (165, 'Palau', 'PW', '+680', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (166, 'Palestinian Territory, Occupied', 'PS', '+970', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (167, 'Panama', 'PA', '+507', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (168, 'Papua New Guinea', 'PG', '+675', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (169, 'Paraguay', 'PY', '+595', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (170, 'Peru', 'PE', '+51', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (171, 'Philippines', 'PH', '+63', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (172, 'Pitcairn', 'PN', '+872', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (173, 'Poland', 'PL', '+48', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (174, 'Portugal', 'PT', '+351', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (175, 'Puerto Rico', 'PR', '+1 939', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (176, 'Qatar', 'QA', '+974', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (177, 'Romania', 'RO', '+40', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (178, 'Russia', 'RU', '+7', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (179, 'Rwanda', 'RW', '+250', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (180, 'Reunion', 'RE', '+262', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (181, 'Saint Barthelemy', 'BL', '+590', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (182, 'Saint Helena, Ascension and Tristan Da Cunha', 'SH', '+290', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (183, 'Saint Kitts and Nevis', 'KN', '+1 869', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (184, 'Saint Lucia', 'LC', '+1 758', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (185, 'Saint Martin', 'MF', '+590', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (186, 'Saint Pierre and Miquelon', 'PM', '+508', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (187, 'Saint Vincent and the Grenadines', 'VC', '+1 784', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (188, 'Samoa', 'WS', '+685', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (189, 'San Marino', 'SM', '+378', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (190, 'Sao Tome and Principe', 'ST', '+239', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (191, 'Saudi Arabia', 'SA', '+966', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (192, 'Senegal', 'SN', '+221', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (193, 'Serbia', 'RS', '+381', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (194, 'Seychelles', 'SC', '+248', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (195, 'Sierra Leone', 'SL', '+232', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (196, 'Singapore', 'SG', '+65', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (197, 'Slovakia', 'SK', '+421', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (198, 'Slovenia', 'SI', '+386', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (199, 'Solomon Islands', 'SB', '+677', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (200, 'Somalia', 'SO', '+252', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (201, 'South Africa', 'ZA', '+27', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (202, 'South Georgia and the South Sandwich Islands', 'GS', '+500', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (203, 'Spain', 'ES', '+34', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (204, 'Sri Lanka', 'LK', '+94', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (205, 'Sudan', 'SD', '+249', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (206, 'Suriname', 'SR', '+597', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (207, 'Svalbard and Jan Mayen', 'SJ', '+47', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (208, 'Swaziland', 'SZ', '+268', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (209, 'Sweden', 'SE', '+46', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (210, 'Switzerland', 'CH', '+41', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (211, 'Syrian Arab Republic', 'SY', '+963', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (212, 'Taiwan', 'TW', '+886', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (213, 'Tajikistan', 'TJ', '+992', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (214, 'Tanzania, United Republic of Tanzania', 'TZ', '+255', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (215, 'Thailand', 'TH', '+66', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (216, 'Timor-Leste', 'TL', '+670', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (217, 'Togo', 'TG', '+228', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (218, 'Tokelau', 'TK', '+690', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (219, 'Tonga', 'TO', '+676', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (220, 'Trinidad and Tobago', 'TT', '+1 868', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (221, 'Tunisia', 'TN', '+216', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (222, 'Turkey', 'TR', '+90', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (223, 'Turkmenistan', 'TM', '+993', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (224, 'Turks and Caicos Islands', 'TC', '+1 649', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (225, 'Tuvalu', 'TV', '+688', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (226, 'Uganda', 'UG', '+256', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (227, 'Ukraine', 'UA', '+380', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (228, 'United Arab Emirates', 'AE', '+971', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (229, 'United Kingdom', 'GB', '+44', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (230, 'United States', 'US', '+1', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (231, 'Uruguay', 'UY', '+598', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (232, 'Uzbekistan', 'UZ', '+998', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (233, 'Vanuatu', 'VU', '+678', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (234, 'Venezuela, Bolivarian Republic of Venezuela', 'VE', '+58', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (235, 'Vietnam', 'VN', '+84', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (236, 'Virgin Islands, British', 'VG', '+1 284', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (237, 'Virgin Islands, U.S.', 'VI', '+1 340', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (238, 'Wallis and Futuna', 'WF', '+681', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (239, 'Yemen', 'YE', '+967', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (240, 'Zambia', 'ZM', '+260', NULL, NULL);
INSERT INTO `countries` (`id_country`, `name`, `code`, `dial_code`, `created_at`, `updated_at`) VALUES (241, 'Zimbabwe', 'ZW', '+263', NULL, NULL);
